<footer>
	<div class="main-content">
		<div class="left box">
			<h2>About us</h2>
			<div class="content">
				<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				<div class="social">
					<a href="#"><span class="fab fa-facebook-f"></span></a>
					<a href="#"><span class="fab fa-twitter"></span></a>
					<a href="#"><span class="fab fa-instagram"></span></a>
					<a href="#"><span class="fab fa-youtube"></span></a>
				</div>
			</div>
		</div>
		<div class="center box">
			<h2>Adress</h2>
			<div class="content">
				<div class="place">
					<span class="fas fa-map-marker-alt"></span>
					<span class="text">Florianópolis, SC - Brasil</span>
				</div>
				<div class="phone">
					<span class="fas fa-phone-alt"></span>
					<span class="text">Telefone</span>
				</div>
				<div class="email">
					<span class="fas fa-envelope"></span>
					<span class="text">mmarinaneri@gmail.com</span>
				</div>
			</div>
		</div>
		<div class="rigth box">
			<h2>Contato</h2>
			<div class="content">
				<form action="#">
					<div class="email">
                		<div class="text">Email *</div>
						<input type="email" required>
              		</div>
					<div class="msg">
                		<div class="text">Menssagem *</div>
                		<textarea rows="2" cols="25" required=""></textarea>
              		</div>
					<div class="bottom">
						<button type="submit">Enviar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</footer>