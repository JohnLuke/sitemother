<!DOCTYPE html>
<html>
<head>
	<title>Marina Neri Machado</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/style.css">
	<link  rel="shortcut icon"  href="./images/favicon.ico">
	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body>
	<div id="general">
		<div id="top">
			<?php include 'top.php'; ?>
		</div>
		<div id="menu">
			<?php include 'menu.php'; ?>
		</div>
		<div id="content">
			<div id="main-content">
				<div class="posts">
					<h1 class="titles">Photo</h1>
					<img class="image" src="images/mae1.jpg">
					<p class="paragrafe">O Lorem Ipsum tem vindo a ser o texto.</p>
					<span class="date">30/05/2020</span>
				</div>

				<div class="posts">
					<h1 class="titles">Photo</h1>
					<img class="image" src="images/mae2.jpg">
					<p class="paragrafe">O Lorem Ipsum tem vindo a ser o texto.</p>
					<span class="date">30/05/2020</span>
				</div>
			</div>
			<div id="recent">
				<div id="sponsored">
					<div class="mrv"> 
						<img class="sponsored-img" src="images/mrv.png">
						<p>Saia já do Aluguel | MRV 40 Anos</p>
						<a href="https://www.mrv.com.br/" target="_blank">mrv.com.br</a>
						<p class="descrition">Maior construtora de imóveis residenciais da América Latina e a única presente em mais de 160 cidades. </p>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<?php include 'footer.php'; ?>
		</div>
	</div> <!-- div general -->

</body>
</html>